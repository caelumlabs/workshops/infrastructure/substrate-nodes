- [Description](#description)
- [Installation](#installation)
  - [Install docker](#install-docker)
- [How to run a simple full node](how-to-run-a-simple-full-node)
- [How to run a validator node](how-to-run-a-validator-node)
  - [Create substrate keys](#create-substrate-keys)
  - [Run your validator node](#run-your-validator-node)
    - [Bond DEV tokens](#bond-dev-tokens)
    - [Set Session Keys](#set-session-keys)
    - [Restart your node](#restart-your-node)
- [Monitoring Tools](#monitoring-tools)

# Description

Workshop to deploy substrate nodes in Lorena blockchain network

# Installation

## Install docker

[Documentation]: https://docs.docker.com/install
[Post Installation]: https://docs.docker.com/install/linux/linux-postinstall/

```
sudo apt-get update
sudo apt install docker.io
sudo systemctl start docker
docker --version
```

If you don’t want to preface the docker command with sudo, create a Unix group called docker and add users to it. When the Docker daemon starts, it creates a Unix socket accessible by members of the docker group.

```
sudo groupadd docker
sudo usermod -aG docker

# logout & login
```

* Download CaelumLabs Substrate Docker image

```
docker pull eu.gcr.io/caelumlabs-hub01/substrate-apps/lorena-radices:workshop_latest
```

# How to run a simple full node 

Change the second name value using the **--name** parameter with a random name (that you remember).

```
docker run --rm -it -p 30333:30333/tcp -p 30333:30333/udp -p 9944:9944/tcp \
  --name workshop-node1 eu.gcr.io/caelumlabs-hub01/substrate-apps/lorena-radices:workshop_latest \
  --chain staging  \
  --base-path /tmp/workshop \
  --name workshop-validator-${MyName} \
  --bootnodes /ip4/34.76.112.106/tcp/30333/p2p/QmZtoYC1dwdY2bDjJYXeE9EWSsyZP5rvpj4TkdyYojH48o 
```

# How to run a validator node

## Create substrate keys

[Documentation]: https://wiki.polkadot.network/docs/en/learn-keys

To create a validator node we will create 3 keys:

**Please, take into account the keypair crypto type before creating it. Stash and Controller use sr25519 and session ed25519.**
 
* **Stash Account (keypair crypto type: sr25519)**: The validator wallet with the funds bonded for staking.
* **Controller Account (keypair crypto type: sr25519)**: Account to start/stop validating and nominate.
* **Session Account (keypair crypto type: ed25519)**: hot keys that be must kept online by a validator to perform network operations.

We can create it following these steps:

  - Open [PolkadotJS]: https://polkadotjs.caelumlabs.com/#/settings

  - Enable Custom endpoint and add our substrate workshop network as "Remote Node/Endpoint"
  
  ```
  wss://substrate-workshop.caelumlabs.com/
  ```
    
  - Create Stash Account (**Save your Mnemonic Seed**)
  - ![STASH](images/workshop-account-stash.png)

  - Create Controller Account (**Save your Mnemonic Seed**)
  - ![CONTROLLER](images/workshop-account-controller.png)

  - Create Session Account (**Save your Raw Seed**)
  - Is important create it with a Raw Seed option (no Mnemonic seed) and crypto type ed25519

  - ![SESSION](images/workshop-account-session.png)

  - In order to activate the node, tokens are needed.
  - Tokens can be earned by sending the stash and controller public keys to albert@caelumlabs.com (One should receive 100Pdevs for stash and 10Pdevs for controller account).

## Run your validator node

Replace the **--node-key** value with the seed key number of the session key created before (without the "0x" prefix).

Also change the second name value using the **--name** parameter with a random name (that you remember).

```
docker run --rm -it -p 30333:30333/tcp -p 30333:30333/udp -p 9944:9944/tcp \
  --name workshop-node1 eu.gcr.io/caelumlabs-hub01/substrate-apps/lorena-radices:workshop_latest \
  --chain staging  \
  --base-path /tmp/workshop \
  --name workshop-validator-${MyName} \
  --bootnodes /ip4/34.76.112.106/tcp/30333/p2p/QmZtoYC1dwdY2bDjJYXeE9EWSsyZP5rvpj4TkdyYojH48o \
  --node-key bc7XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
```

### Bond DEV tokens

It is time to set up our validator from Polkadot JS:

- First, go to the Staking section. Click on "Account Actions", and then the "New stake" button.
- ![BONDING](images/bonding.png) 

### Set Session Keys
- Second, also in the staking section, click on "Account Actions" -> "Set Session Key" above our stake.
- ![SETSESSION1](images/sessionkey1.png) 
- ![SETSESSION2](images/sessionkey2.png) 

### Restart your node

Once your node is fully synced, stop the process by pressing Ctrl-C. At the terminal prompt, you will now start running the node in validator mode.

```
docker run --rm -it -p 30333:30333/tcp -p 30333:30333/udp -p 9944:9944/tcp \
  --name workshop-node1 eu.gcr.io/caelumlabs-hub01/substrate-apps/lorena-radices:workshop_latest \
  --chain staging  \
  --base-path /tmp/workshop \
  --name workshop-validator-${MyName} \
  --bootnodes /ip4/34.76.112.106/tcp/30333/p2p/QmZtoYC1dwdY2bDjJYXeE9EWSsyZP5rvpj4TkdyYojH48o \
  --node-key bc7XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  --validator
```

# Monitoring Tools

- Polkadot JS - Blockchain Explorer : https://polkadotjs.caelumlabs.com/
- Telemetry - Nodes Explorer: https://telemetry.polkadot.io/#list/Staging%20Testnet
